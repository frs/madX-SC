# |
# o---------------------------------------------------------------------o
# |
# | MAD makefile - post-makefile linux cutomization
# |
# o---------------------------------------------------------------------o
# |
# | Methodical Accelerator Design
# |
# | Copyright (c) 2011+ CERN, mad@cern.ch
# |
# | For more information, see http://cern.ch/mad
# |
# o---------------------------------------------------------------------o
# |
# | $Id$
# |

#
# all
#
all-linux:         all-linux32        all-linux64
all-linux-gnu:     all-linux32-gnu    all-linux64-gnu
all-linux-intel:   all-linux32-intel  all-linux64-intel

all-linux32:       madx-linux32 libmadx-linux32 libptc-linux32 numdiff-linux32
all-linux64:       madx-linux64 libmadx-linux64 libptc-linux64 numdiff-linux64

all-linux32-gnu:   madx-linux32-gnu libmadx-linux32-gnu libptc-linux32-gnu numdiff-linux32-gnu
all-linux64-gnu:   madx-linux64-gnu libmadx-linux64-gnu libptc-linux64-gnu numdiff-linux64-gnu

all-linux32-intel: madx-linux32-intel libmadx-linux32-intel libptc-linux32-intel numdiff-linux32-intel
all-linux64-intel: madx-linux64-intel libmadx-linux64-intel libptc-linux64-intel numdiff-linux64-intel

#
# cleanbuild platform specific
#
cleanarch:
	$E "** Cleaning binaries and libraries"
	$_ $(RM) $(wildcard madx32 madx64 numdiff32 numdiff64 libmadx32* libmadx64* libptc32* libptc64*)
	$_ $(RM) $(wildcard madx-linux* libmadx-linux* libptc-linux* numdiff-linux* libs/gc/libgc*.a)

#
# madx
#
madx-linux:         madx-linux32         madx-linux64
madx-linux-gnu:     madx-linux32-gnu     madx-linux64-gnu
madx-linux-intel:   madx-linux32-intel   madx-linux64-intel
madx-linux-lahey:   madx-linux32-lahey   madx-linux64-lahey
madx-linux-nagfor:  madx-linux32-nagfor  madx-linux64-nagfor

.PHONY:  madx-linux32         madx-linux64
.PHONY:  madx-linux32-gnu     madx-linux64-gnu
.PHONY:  madx-linux32-intel   madx-linux64-intel
.PHONY:  madx-linux32-lahey   madx-linux64-lahey
.PHONY:  madx-linux32-nagfor  madx-linux64-nagfor

madx-linux%: MAKE_ARGS = PRJNAME=$@ DESTDIR=$(DESTDIR) ONLINE=yes STATIC=yes USEGC=yes APPENDLD=yes
madx-linux%: MAKE_OPTS = -j9 $(MAKEFLAGS) $(MAKEOVERRIDES)
madx-linux%: MAKE_LINK = $(if $(DESTDIR),ln -sf $(DESTDIR)$@ &&,) ln -sf $@ 

madx-linux32-intel madx-linux32: libs/gc/libgc-linux32-intel.a
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 COMP=intel && $(MAKE_LINK) madx32

madx-linux64-intel madx-linux64: libs/gc/libgc-linux64-intel.a
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 COMP=intel && $(MAKE_LINK) madx64

madx-linux32-gnu: libs/gc/libgc-linux32-gnu.a
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 COMP=gnu && $(MAKE_LINK) madx32

madx-linux64-gnu: libs/gc/libgc-linux64-gnu.a
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 COMP=gnu && $(MAKE_LINK) madx64

madx-linux32-nagfor: libs/gc/libgc-linux32-gnu.a
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 FC=nagfor && $(MAKE_LINK) madx32

madx-linux64-nagfor: libs/gc/libgc-linux64-gnu.a
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 FC=nagfor && $(MAKE_LINK) madx64

madx-linux32-lahey: libs/gc/libgc-linux32-gnu.a
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 FC=lf95 && $(MAKE_LINK) madx32

madx-linux64-lahey: libs/gc/libgc-linux64-gnu.a
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 FC=lf95 && $(MAKE_LINK) madx64

#
# libgc
#
libs/gc/libgc-linux%: MAKE_OPTS = -j9 --no-print-directory -C libs/gc

libs/gc/libgc-linux32-intel.a:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_OPTS) libgc-intel32

libs/gc/libgc-linux64-intel.a:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_OPTS) libgc-intel64;

libs/gc/libgc-linux32-gnu.a:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_OPTS) libgc-gnu32

libs/gc/libgc-linux64-gnu.a:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_OPTS) libgc-gnu64

#
# libmadx
#
libmadx-linux:        libmadx-linux32        libmadx-linux64
libmadx-linux-gnu:    libmadx-linux32-gnu    libmadx-linux64-gnu
libmadx-linux-intel:  libmadx-linux32-intel  libmadx-linux64-intel

libmadx-linux%: DESTDIR   := $(DESTDIR)libs/madx
libmadx-linux%: MAKE_ARGS  = PRJNAME=$@ DESTDIR=../../$(DESTDIR) ONLINE=no STATIC=yes USEGC=yes APPENDLD=yes
libmadx-linux%: MAKE_OPTS  = -j9 --no-print-directory -C libs/madx $(MAKEFLAGS) $(MAKEOVERRIDES)
libmadx-linux%: MAKE_LINK1 = ln -sf $(DESTDIR)/$@.a  && ln -sf $@.a
libmadx-linux%: MAKE_LINK2 = ln -sf $(DESTDIR)/$@.so && ln -sf $@.so

libmadx-linux32-intel libmadx-linux32:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 COMP=intel && $(MAKE_LINK1) libmadx32.a && $(MAKE_LINK2) libmadx32.so

libmadx-linux64-intel libmadx-linux64:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 COMP=intel && $(MAKE_LINK1) libmadx64.a && $(MAKE_LINK2) libmadx64.so

libmadx-linux32-gnu:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 COMP=gnu && $(MAKE_LINK1) libmadx32.a && $(MAKE_LINK2) libmadx32.so

libmadx-linux64-gnu:
	$E "*** Building $@"                                # STATIC=no due to missing -fPIC in the runtime library of gfortran 64 bit
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 COMP=gnu STATIC=no && $(MAKE_LINK1) libmadx64.a && $(MAKE_LINK2) libmadx64.so

#
# libptc
#
libptc-linux:         libptc-linux32        libptc-linux64
libptc-linux-gnu:     libptc-linux32-gnu    libptc-linux64-gnu
libptc-linux-intel:   libptc-linux32-intel  libptc-linux64-intel

libptc-linux%: DESTDIR   := $(DESTDIR)libs/ptc
libptc-linux%: MAKE_ARGS  = PRJNAME=$@ DESTDIR=../../$(DESTDIR) STATIC=yes APPENDLD=yes
libptc-linux%: MAKE_OPTS  = -j9 --no-print-directory -C libs/ptc $(MAKEFLAGS) $(MAKEOVERRIDES)
libptc-linux%: MAKE_LINK1 = ln -sf $(DESTDIR)/$@.a  && ln -sf $@.a
libptc-linux%: MAKE_LINK2 = ln -sf $(DESTDIR)/$@.so && ln -sf $@.so

libptc-linux32-intel libptc-linux32:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 COMP=intel && $(MAKE_LINK1) libptc32.a && $(MAKE_LINK2) libptc32.so

libptc-linux64-intel libptc-linux64:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 COMP=intel && $(MAKE_LINK1) libptc64.a && $(MAKE_LINK2) libptc64.so

libptc-linux32-gnu:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 COMP=gnu && $(MAKE_LINK1) libptc32.a && $(MAKE_LINK2) libptc32.so

libptc-linux64-gnu:
	$E "*** Building $@"                                # STATIC=no due to missing -fPIC in the runtime library of gfortran 64 bit
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 COMP=gnu STATIC=no && $(MAKE_LINK1) libptc64.a && $(MAKE_LINK2) libptc64.so

#
# numdiff
#
numdiff-linux:        numdiff-linux32        numdiff-linux64
numdiff-linux-gnu:    numdiff-linux32-gnu    numdiff-linux64-gnu
numdiff-linux-intel:  numdiff-linux32-intel  numdiff-linux64-intel

numdiff-linux%: DESTDIR  := $(DESTDIR)tools/numdiff
numdiff-linux%: MAKE_ARGS = PRJNAME=$@ DESTDIR=../../$(DESTDIR) STATIC=yes APPENDLD=yes
numdiff-linux%: MAKE_OPTS = -j9 --no-print-directory -C tools/numdiff $(MAKEFLAGS) $(MAKEOVERRIDES)
numdiff-linux%: MAKE_LINK = ln -sf $(DESTDIR)/$@ && ln -sf $@

numdiff-linux32-intel numdiff-linux32:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 COMP=intel && $(MAKE_LINK) numdiff32

numdiff-linux64-intel numdiff-linux64:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 COMP=intel && $(MAKE_LINK) numdiff64

numdiff-linux32-gnu:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=32 COMP=gnu && $(MAKE_LINK) numdiff32

numdiff-linux64-gnu:
	$E "*** Building $@"
	$_ $(MAKE) $(MAKE_ARGS) $(MAKE_OPTS) ARCH=64 COMP=gnu && $(MAKE_LINK) numdiff64

# end of makefile
