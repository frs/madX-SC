#ifndef MAD_ELEMMULTP_H
#define MAD_ELEMMULTP_H

// types

struct element;

// interface

double mult_par(const char* par, struct element* el);

#endif // MAD_ELEMMULTP_H

