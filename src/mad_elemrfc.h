#ifndef MAD_ELEMRFC_H
#define MAD_ELEMRFC_H

void    print_rfc(void);
void    adjust_rfc(void);
double  rfc_slope(void);

#endif // MAD_ELEMRFC_H

