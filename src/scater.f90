MODULE matnam
  implicit none
  CHARACTER*12 :: material_names(10)   !VVK's
!  COMMON /materials_10/ material_names !VVK's common from MATER
end MODULE matnam
MODULE TARGB_STRUCT
!--------------------------------------------------------------------!
! This module has been created by V.Kapin (FNAL) in 2015             !
! to simulate interactions of protons with the thin foil             !
! of primary collimator.                                             !
!                                                                    !
! This module has been created owing to advises and discussions with !
! N.Mokhov and S. Striganov (FNAL).                                  !
!                                                                    !
! The module uses original subroutines of the STRUCT code            !
! by A.Drozhdin & N. Mokhov(FNAL)                                    !
!(Web-site http://www-ap.fnal.gov/users/drozhdin/STRUCT/)            !
!                                                                    !
! Two new fortran-95 subroutines "prepare_primcolls"                 !
! and "interact_primcolls" of this module serves                     !
! as an inteface for two original FORTRAN-66/77 subroutines          !
! "PREPEB" and "TARGB" of the STRUCT (file hbs.f), respectively.     !
!                                                                    !
! Originally (~2000), those subroutines have been transferred        !
! into STRUCT from earliest versions of the MARS code                !
! by N.Mokhov (FNAL) (Web-site http://www-ap.fnal.gov/MARS/)         !
!--------------------------------------------------------------------!

!USE
SAVE
PRIVATE

PUBLIC :: prepare_primcolls !envelope for PREPEB
PUBLIC :: interact_primcoll !envelope for TARGB
!PUBLIC :: RNDM5 ! check if it is needed

double precision, parameter :: E_proton_rest_energy_GeV=0.938272013D0 !as mad_dic.c
                     !E_proton_rest_energy_GeV=0.93828D0 !as in my targb_vk
                     
double precision, public    :: beta_beam !calculated in subr. <<prepare_primcolls>>

!-------------------------------------------------------
CONTAINS 
  subroutine prepare_primcolls &
    (current_turn_number, name_len, matter_name, P0_beam_momentum_GeV_c, dp_limit)		
    use matnam
    implicit none
    integer, intent(IN) :: current_turn_number !to restrict printing 
    integer, intent(IN) :: name_len	 
	character(name_len), intent(IN) :: matter_name
	double precision, intent (IN) :: P0_beam_momentum_GeV_c
    double precision, intent (INOUT) :: dp_limit !0.7 - normally used in STRUCT


    integer :: i_used_material

DOUBLE PRECISION ::    ZIM, AIM, ROM,&   
              BET, DHB,         &    
			  ZA,AT,DENS,FLAN,              &
			  WDIAM,WDIST,FEKVCM,FMTESL,DPPLIM
INTEGER ::            IABS,  IMELT 

    COMMON/MATI/ZIM,AIM,ROM
    COMMON/IABS/BET,DHB,IABS                                   
    COMMON/BLMAT/ZA(10),AT(10),DENS(10),FLAN(10),IMELT(10) 
    COMMON/BLFDAT/WDIAM,WDIST,FEKVCM,FMTESL,DPPLIM

	!local variables
	integer :: i  
      !logical debug 
      !common/vkapin/ debug
      !debug=.false. !.true. !.false.

    beta_beam=1./sqrt(1.+(E_proton_rest_energy_GeV/P0_beam_momentum_GeV_c)**2)

        if (dp_limit.le.0) then 
	  dp_limit=0.7D0; 
	  Print *, '!!! Since dp_limit=0, it set to its default value 0.7'
	endif
	DPPLIM=dp_limit
        ! if (debug) Print *,'DPPLIM=', DPPLIM

    CALL PREPEB(P0_beam_momentum_GeV_c)

       !if (debug) Print *, 'subr. prepare_primcolls:  after CALL PREPEB'
       !if (debug) Print *, 'WDIAM,WDIST,FEKVCM,FMTESL,DPPLIM=', &
       !                     WDIAM,WDIST,FEKVCM,FMTESL,DPPLIM

    i_used_material=0
	do i=1,10
	  !Print *, TRIM(material_names(i))
	  if(TRIM(matter_name) .eq. TRIM(material_names(i)) ) i_used_material=i
	enddo

	  if (i_used_material.eq.0) then
	     Print *, '! the submitted material is not available ! STOP! '
             STOP
          elseif(current_turn_number.eq.1) then 
             Print *,'------------------------------------------------------------'
             Write(*,"(1x,'   The collimator material is <<',A,'>>'/1x,'   In the above table its number=',i2)") &
               TRIM(material_names(i_used_material)), i_used_material 
             Print *,'============================================================'
         
	  endif

      !for COMMON/MATI/ZIM,AIM,ROM
	     ZIM=ZA(i_used_material)   ! Material_Atomic_Number_dimless
	     AIM=AT(i_used_material)   !Material_Atomic_Mass_amu
         ROM=DENS(i_used_material) !Material_Density_g/cm3
	  	 DHB=FLAN(i_used_material) ! value calculated in PREPEB
         ! if (debug) Print *, 'Material: ZIM, AIM, ROM, DHB =', &
		 !                                ZIM, AIM, ROM, DHB

END subroutine prepare_primcolls
!===============================================================
      SUBROUTINE PREPEB(PO)
!- - - - - - - - - - - - - - - - - - - - - - -  
!  DATA INPUT & PRECALCULATION FOR SCELTB
!- - - - - - - - - - - - - - - - - - - - - - -
!   Modified: 04-MAR-2001 by NVM

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
!VK   COMMON/NOBINIT/NOBINI    !  initial turn number for continue run
!VK not used =========================================================
       COMMON                                           &
      /BLMAT/ZA(10),AT(10),DENS(10),FLAN(10),IMELT(10)  &
      /BLBUF/ZBPRO,ZBES,ZBTAR,ZBEMS,ZBSPAN,ABPLE,INQWIR &
      /BLFDAT/WDIAM,WDIST,FEKVCM,FMTESL,DPPLIM          &
      /BLIV/RL,SR,IV
!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
!vk=======================================================
      double precision :: PO,ZBPRO,ZBES,ZBTAR,ZBEMS,ZBSPAN,ABPLE, &
                 BET2,ZA,DPPLIM,ODIN,AT,POT,SN,ZS,B,C,   &
                 DENS,Y1,CUP,DC,Y0,YP,DELT,DHB,FLAN,     &
                 WDIAM,WDIST,FEKVCM,FMTESL,RL,SR
      integer :: IV,INQWIR,NMAT,IM,IMELT,iii   
      dimension ODIN(10) !in f90 is array required 
      logical debug 
      common/vkapin/ debug
!vk=======================================================

      IV=0

      INQWIR=1
      ZBPRO=0.
      ZBES=0.
      ZBTAR=0.
      ZBEMS=0.
      ZBSPAN=0.
      ABPLE=1.
      BET2=PO*PO/(PO*PO+0.93828*0.93828)

      if (debug) then 
	  Print *,'---------------------------------------------------'
	  Print *,'In SUBROUTINE PREPEB(PO):'
       Print *,'IV,INQWIR,ZBPRO,ZBES,ZBTAR,ZBEMS,ZBSPAN,ABPLE,BET2=', &
                IV,INQWIR,ZBPRO,ZBES,ZBTAR,ZBEMS,ZBSPAN,ABPLE,BET2
	endif

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	  if (debug) Print *,'In SUBR. PREPEB: CALL VZEROB(ZA,50)'

      CALL VZEROB(ZA,50)
!- - - - - - - DEFAULT DEFINITIONS - - - - - - - - - -
	  if (debug) Print *,'In SUBR. PREPEB: DEFAULT DEFINITIONS:'
 
!VK   DPPLIM=0.7
      ! DPPLIM=0.7D0  VK-move to calling subroutine
      NMAT=10 !VK20150315 NMAT=7
        if (debug) Print *,'DPPLIM, NMAT=', DPPLIM, NMAT
        if (debug) Print *,'In subr. PREPEB: before CALL MATER:', &
        '-----------------------'
      CALL MATER
        if (debug) Print *,'In subr. PREPEB: after CALL MATER:', &
        '-----------------------'
! - - - - - -  - - - - - - - - - - - - - - - - - - - - 
        if (debug) Print *,                                      &
       'In subr. PREPEB: CALCULATION FOR LANDAU DISTRIBUTION:'
! + + + CALCULATION FOR LANDAU DISTRIBUTION + + +
        DO 22 IM=1,NMAT
        IMELT(IM)=IM
        ODIN=1.
        CALL POTIB(ZA(IM),ODIN,AT(IM),1,POT,SN,ZS)
        B=DLOG(5.E11/POT**2)
        C=2.*DLOG(POT/(28.8*SQRT(SN*DENS(IM))))+1.
        IF(POT-100.)1,1,2
    1   Y1=2.
        CUP=3.681
        DC=1.
        GO TO 3
    2   Y1=3.
        CUP=5.215
        DC=1.5
    3   IF(C-CUP)4,4,5
    4   Y0=0.2
        GO TO 6
    5   Y0=0.326*C-DC
    6   YP=DLOG10(PO/0.93828)
        IF(YP-Y1)7,7,8
    7   DELT=-C+4.606*(C/4.606-Y0)*((Y1-YP)/(Y1-Y0))**3
        GO TO 9
    8   DELT=-C
    9   DHB=B+1.116-BET2-DELT
! + + + + + + + + + + + + + + + + + + + + + + + + + + +
   21   FLAN(IM)=DHB
   22   CONTINUE

      if (debug) then 
	  Print *, 'Subr. PREPEB: Assigned COMMON/BLMAT/ parameters:'
	  Print *,                                               &
       '== i  ZA(i)    AT(i)    DENS(i)  FLAN(i) IMELT(i)'
	  do iii=1,NMAT
	   Write(6,95) iii,ZA(iii),AT(iii),DENS(iii),FLAN(iii),IMELT(iii)
   95    format(1x,i3,1x,(4G10.3),1x,I2)
	  enddo
	  Print *,                                                    &
       '--------------------------------------------------------'
        Print *,                                                  &
        'PREPEB: RETURN ==========================================' 
      endif

      RETURN
END SUBROUTINE PREPEB
!============================================================
      SUBROUTINE VZEROB(A,N)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
!VK   COMMON/NOBINIT/NOBINI    !  initial turn number for continue run
!VK not used in this subr.
      double precision :: A
      integer :: N,I
      DIMENSION A(N)
      DO 1 I=1,N
    1 A(I)=0.d0
      RETURN
END SUBROUTINE VZEROB
!     ***************************************************************

!============================================================
      subroutine MATER
        use matnam
!     material definition for scattering elements
!VVK 20150313 rearanged for 10 materials
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON                                          &
      /BLMAT/ZA(10),AT(10),DENS(10),FLAN(10),IMELT(10) &
      /BLFDAT/WDIAM,WDIST,FEKVCM,FMTESL,DPPLIM

!vk========================================================
    double precision :: ZA,AT,DENS,WDIAM,WDIST,FEKVCM,FMTESL,FLAN, &
               DPPLIM
    integer :: iii,IMELT,im 
    logical, save :: print_all_materials=.TRUE.
!	CHARACTER*12 :: material_names(10)='not_set_yet!'
!	COMMON /materials_10/ material_names 
	logical :: debug
    common/vkapin/ debug
        integer :: i_turn !to restrict printing
        common /turn_number/ i_turn
!vk========================================================
       !Print *, 'material_names=', material_names
!  ZA(IM), AT(IM), DENS(IM) - are parameters of material

! No.1  graphite  **********
        material_names(10)='not_set_yet!'
	material_names(1)='graphite'
      ZA(1)=6.
      AT(1)=12.01
      DENS(1)=1.75

! No.2 steel  **********
	material_names(2)='steel'
      ZA(2)=26.
      AT(2)=55.85
      DENS(2)=7.87

! No.3 titanium  **********
	material_names(3)='titanium'
      ZA(3)=22.
      AT(3)=47.88
      DENS(3)=4.54

! No.4 copper ********
	material_names(4)='copper'
      ZA(4)=29.0D0
      AT(4)=63.546D0
      DENS(4)=8.96D0

! No.5 tungsten  **********
	material_names(5)='tungsten'
       ZA(5)=74.
       AT(5)=183.85
       DENS(5)=19.3

! No.6 beryllium  **********
	material_names(6)='beryllium'
      ZA(6)=4.
      AT(6)=9.01
      DENS(6)=1.85

! No.7  aluminum *******
	material_names(7)='aluminum'
      ZA(7)=13.
      AT(7)=26.98
      DENS(7)=2.70

! No.8 silicon   **********
	material_names(8)='silicon'
      ZA(8)=14.
      AT(8)=28.09
      DENS(8)=2.33

! No.9 scintilator   **********
	material_names(9)='scintilator'
      ZA(9)=5.2
      AT(9)=10.24
      DENS(9)=1.03

! No.10 gold   **********
	material_names(10)='gold'
      ZA(10)=79.0
      AT(10)=196.967
      DENS(10)=19.32

      WDIAM=0.1
      WDIST=1.5
      FEKVCM=80.
      FMTESL=1.D0
! TEV WDIAM=0.05
! TEV WDIST=2.5



      if (debug) then 
	  Print *, 'Subr. MATER: Assigned COMMON parameters:'
	  Print *,                                                       &
       '== i  ZA(i)    AT(i)    DENS(i)  FLAN(i) IMELT(i) ! name =='
	  do iii=1,10
	   Write(6,95) iii,ZA(iii),AT(iii),DENS(iii),FLAN(iii), &
                       IMELT(iii), material_names(iii)
   95    format(1x,i3,1x,(4G10.3),1x,I2,1x,'  !',1x,A8)
	  enddo
	  Print *,                                                   &
       '--------------------------------------------------------'
	  Print *, 'WDIAM, WDIST, FEKVCM, FMTESL =',  &
                WDIAM, WDIST, FEKVCM, FMTESL
        Print *,                                                 &
        'MATER: RETURN =========================================='
        
      endif

      if (print_all_materials) then
          Print *
	  Print *,'--- 10 materials available for collimators -----'
	  Print *,'   Material       Atomic     Atomic     Density ' 
	  Print *,'number & name     number     mass[amu]  [g/cm^3]' 
	  Print *,'------------------------------------------------'
	DO im=1,10 !print 10 materials
	  Write(*,"(1x,I2,2x,A12,1x,F7.3,3x,F9.3,2x,F9.3)") &
            im,material_names(im),ZA(im),AT(im),DENS(im)
        ENDDO
	  Print *,'------------------------------------------------'
          Print *
      endif
      print_all_materials=.FALSE. !Print only at first call !

      return
end subroutine MATER

!============================================================
      SUBROUTINE POTIB(Z,W,A,ND,POT,SN,ZS)
!----------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      !vk=============================================
      double precision :: PO,Z,W,A,POT,SN,ZS,SM,SN1,SS
      integer :: ND,N,I 
      logical :: debug=.FALSE. ! .TRUE. 
      !common/vkapin/ debug
      !vk=============================================

      !COMMON/NOBINIT/NOBINI    !  initial turn number for continue run
      DIMENSION Z(ND),W(ND),A(ND)
      DIMENSION PO(13)
      
      DATA PO/18.7d0,42.d0,39.d0,60.d0,68.d0,78.d0,99.5d0,98.5d0,117.d0,140.d0,150.d0,157.d0,163.d0/
! - - - - - -
       if(debug) print *, 'subr. POTIB(Z,W,A,ND,POT,SN,ZS)'
! 	   if(debug) print *, 'ND=', ND 
      N=0
      SM=0
      SN1=0
      SS=0
5     N=N+1
!      if(debug)  print *, 'SM, SN1, SS, 5 N=', SM, SN1, SS, N
      I=0
1     I=I+1
!      if(debug)  print *, '1  I=I+1=',I,' Z(N)=', Z(N)        
      IF(Z(N) .EQ. I) GO TO 2
!      if(debug)  print *, 'after IF() GO TO 2'
      IF(I-13)1,4,4
4     CONTINUE
!      if(debug)  print *, 'after 4 CONTINUE'
      ZS=9.76*Z(N)+58.8*Z(N)**(-0.19)
!	  if(debug)print *, 'ZS=', ZS, ' GO TO 3'
      GO TO 3
2     ZS=PO(I)
!        if(debug)print *, '2     ZS=PO(I)=', ZS   
3     SM=SM+W(N)*Z(N)/A(N)
!        if(debug)print *, 'Z(N),W(N),A(N)=', Z(N),W(N),A(N), & 
!                          ' SM=', SM, ' SN1=', SN1 
      SN1=SN1+W(N)*Z(N)
!        if(debug)print *, 'SN1==SN1+...=', SN1  
      SS=SS+W(N)*Z(N)*DLOG(ZS)/A(N)
!	    if(debug) print *, 'SS=SS+...=',SS
!        if(debug) print *, 'before IF(N-ND) 5,6,6   N, ND=', N, ND
      IF(N-ND) 5,6,6
6     POT=DEXP(SS/SM)
      ZS=SN1
      SN=SM
        if(debug) print *, 'POT, ZS, SN=', POT, ZS, SN, 'RETURN!'
      RETURN
END SUBROUTINE POTIB

!===================================================================
!===================================================================

  subroutine interact_primcoll                                     &
                        (EL, P0_beam_momentum_GeV_c,               &
                         PX_madx,PY_madx,PT_madx, IPOP_lost_flag)
	double precision, intent (IN)    :: EL ! = target_thickness_m
	double precision, intent (IN)    :: P0_beam_momentum_GeV_c
    double precision, intent (INOUT) :: PX_madx,PY_madx,PT_madx ! z(2),z(4),z(6)
    integer, intent(OUT)    :: IPOP_lost_flag

    DOUBLE PRECISION :: ZIM, AIM, ROM,         TAR,      &
               P,W,XD,YD,ZD,DX,DY,DZ,           &
               BET,             DHB,            &
               WDIAM,WDIST,FEKVCM,FMTESL,DPPLIM
    
    INTEGER ::     IABS 

    COMMON/MATI/ZIM,AIM,ROM
	COMMON/PART/P,W,XD,YD,ZD,DX,DY,DZ !set here: P
    COMMON/IABS/BET,DHB,IABS          !set here: BET; 
    COMMON/BLFDAT/WDIAM,WDIST,FEKVCM,FMTESL,DPPLIM

!common variables for below internal subroutines
       double precision :: TR,AS, AM, ZS, ZM, DENS, RO    !,     &
!                 AK,SPAA,Z13,A67,BCOH,SINC,SCOC,  &
!                 E,P,BET,BET2,SQS,SPA,SPAIR,SPR,  &
!                 SELC,SELI,SEL,BINE,STOT,TRO,     &
!                 RNN11,DT,AKS,RNM11,DELE,DHB,P1,  &
!                 P2,TET,SS,CS,RTYP,RR,U,BEL,TEL2, &
!                 TEL,W,V,EN,TET2,XXX,VX,VY,VZ,F
!      integer :: MINT,IABS  

!local
	!double precision :: beta_beam "common" in this module calculated 
	!                      in subr. <<prepare_primcolls>>
	 double precision :: delta_p, delta_p_plus1,                           &
	            WIN_part_weight_before_target,                    &
				!X_hor_mm, Y_ver_mm,                  &
				XP_hor_angle_mrad, YP_ver_angle_mrad, &
                XP_new_hor_angle_mrad, YP_new_ver_angle_mrad           
     !logical debug 
     !common/vkapin/ debug
     !debug=.false. !.true. !.false.

       if(beta_beam .gt. 0) then
	     delta_p=-1.0D0+DSQRT(1.0D0+2.0D0*PT_madx/beta_beam+PT_madx*PT_madx)
             delta_p_plus1=delta_p+1D0
       else
         Print *, 'beta_beam is appeared as zero. STOP!'
		 STOP
	   end if

       P=P0_beam_momentum_GeV_c*(1.0D0+delta_p) !particle (not beam)
       BET=1./sqrt(1.+(E_proton_rest_energy_GeV/P)**2) !particle (not beam)

      IPOP_lost_flag=0
      WIN_part_weight_before_target=1.0D0 
      W=WIN_part_weight_before_target !W could be changed during interaction
      
                                ! STRUCT coords from MADX
	  XP_hor_angle_mrad=1.0D03*PX_madx/delta_p_plus1  ! 1000*z(2) 
	  YP_ver_angle_mrad=1.0D03*PY_madx/delta_p_plus1  ! 1000*z(4)

      TAR=100.*EL*ROM

      !STRUCT-to-MARS coordinate conversion:
      DZ= 1./SQRT(1.+1.E-6*(XP_hor_angle_mrad*XP_hor_angle_mrad+ &
	                        YP_ver_angle_mrad*YP_ver_angle_mrad))
      DX= 0.001*YP_ver_angle_mrad*DZ
      DY= 0.001*XP_hor_angle_mrad*DZ

      CALL TARGB(TAR)

	  IF(P/P0_beam_momentum_GeV_c.LT.DPPLIM) IPOP_lost_flag=1 !GO TO 1

      XP_new_hor_angle_mrad= 1000.*DY/DZ
      YP_new_ver_angle_mrad= 1000.*DX/DZ

	  IF(RNDM5(-1.).GT.W/WIN_part_weight_before_target) IPOP_lost_flag=1


!     COORDS from STRUCT to MADX
	  delta_p=P/P0_beam_momentum_GeV_c-1.0D0
           delta_p_plus1=delta_p+1D0
          PT_madx= & 
	  (-1.0D0+DSQRT(1.0D0+(2.0D0+delta_p)*delta_p*beta_beam**2))/beta_beam
	  PX_madx=0.001*XP_new_hor_angle_mrad*delta_p_plus1 
	  PY_madx=0.001*YP_new_ver_angle_mrad*delta_p_plus1

END subroutine interact_primcoll

      SUBROUTINE TARGB(TAR)
!=== Modified 25-MAY-2001 by NVM : SCELTB, TARGB, PROUTB ===
!- - - - - - - - - - - - - - - - - - - - -
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
!VK   COMMON/NOBINIT/NOBINI    !  initial turn number for continue run
      COMMON                                    &
      /PART/P,W,XXX(3),VX,VY,VZ                 &
      /IABS/BET,DHB,IABS                        &
      /MATI/ZM,AM,RO                            &
      /EMSC/P1,P2,BET2,ZS,AS,DENS,DT,F,TET2     &
      /BSELP/SQS,E,EN,V,CS,SS
!- - - - - - - - - - - - - - - - - - - - - - 
!vk================================================
      double precision :: TAR,TR,AS, AM, ZS, ZM, DENS, RO, &
                 AK,SPAA,Z13,A67,BCOH,SINC,SCOC,  &
                 E,P,BET,BET2,SQS,SPA,SPAIR,SPR,  &
                 SELC,SELI,SEL,BINE,STOT,TRO,     &
                 RNN11,DT,AKS,RNM11,DELE,DHB,P1,  &
                 P2,TET,SS,CS,RTYP,RR,U,BEL,TEL2, &
                 TEL,W,V,EN,TET2,XXX,VX,VY,VZ,F
      integer :: MINT,IABS   
      logical debug 
      common/vkapin/ debug
!vk================================================

	if (debug) print *
	if (debug) print *, 'Subr. TARGB ----------------------'
	if (debug) print *


      TR=0.
      AS=AM   ! Material_Atomic_Mass   [amu]
      ZS=ZM   ! Material_Atomic_Number [dim-less]
      DENS=RO ! Material_Density       [g/cm^3]
	if (debug) print *,                         &
     	'AM[amu], ZM[dim-less], RO[g/cm^3]=',   & 
         AM,      ZM,           RO
	if (debug) print *,                         &
     	'AS[amu], ZS[dim-less],	DENS[g/cm^3]=', &
         AS,      ZS,           DENS

      AK=0.1536*ZM/AM
      SPAA=8.46E-6*ZM*(ZM+1.)/AM
      Z13=ZM**(1./3.)/183
      A67=(1.-1.25/AM)*6.02E-4/AM**(1./3.)
      BCOH=15.8*AM**0.63
      SINC=21.*AM**0.315*(1.-DEXP(-0.13*AM))*6.02E-4/AM
      SCOC=(ZM-1.23+16./ZM**2)*6.02E-4/AM

	if (debug) print *
	if (debug) print *,                        &
     	'AK, SPAA, Z13,A67,BCOH,SINC,SCOC=',   &
         AK, SPAA, Z13,A67,BCOH,SINC,SCOC

    1 E=P/BET
      BET2=BET*BET
      SQS=SQRT(1.87656*(E+0.93828))
      SPA=2.15-0.97*DLOG(Z13+860./E)

	if (debug) print *
	if (debug) print *,                        &
     	'P, BET, E, BET2, SQS, SPA=',          &
         P, BET, E, BET2, SQS, SPA

      IF(SPA)2,2,3
    2 SPAIR=0.
      GO TO 4
    3 SPAIR=SPAA*SPA*(DLOG(7.*E)-1.)
    4 SPR=A67*(46.79+0.03*SQS)
      SELC=SCOC*(13.5+0.3*DABS(DLOG(E/50.)))
      SELI=SINC*(0.64+3.6/E)
      SEL=SELI+SELC

	if (debug) print *
	if (debug) print *,                               &
     	'SPAIR, SPR, SELC, SEL=', SPAIR, SPR, SELC, SEL 

      if(P)901,901,902
		if (debug) print *, 'P=',P,' if(P<=0) => 901 RETURN' 
 901  return
 902  BINE=9.26-4.94/SQRT(P)+0.28*DLOG(P)
      STOT=SPR+SEL+SPAIR
      TRO=TR
      RNN11=RNDM5(-1.)
	if (debug) print *
	if (debug) print *,                                   &
     	'BINE, STOT, TRO, RNN11=', BINE, STOT, TRO, RNN11

      if(RNN11)701,701,702
		if (debug) print *,                             &
     	'RNN11=',RNN11,' if(RNN11<=0) => 701 RETURN' 
 701  return
 702  TR=-DLOG(RNN11)/STOT+TRO
      MINT=1
		if (debug) print *,                      &
     	'702 TR=', TR, ' MINT=', MINT,           &
         'IF(TR-TAR)6,5,5  TR, TAR, (TR-TAR)=', &
          TR, TAR, (TR-TAR)

      IF(TR-TAR)6,5,5
    5 TR=TAR
      MINT=0
        if (debug) print *,'5 TR, MINT=', TR, MINT

    6 DT=TR-TRO
      AKS=AK*DT/BET2
      RNM11=RNDM5(-1.)
	  if (debug) print *, 'TR, MINT, DT, AKS, RNM11=', &
                           TR, MINT, DT, AKS, RNM11

        if (debug) print *,                         &
       'AKS=',AKS,' if(AKS<=0) RETURN from TARGB'
      if(AKS)601,601,602
 601  return
 602  DELE=1.E-3*AKS*(GL(RNM11)+DLOG(AKS)+DHB )
        if (debug .AND. (DHB.EQ.0)) print *,    &
        '!!! DHB-nucl. length is ZERO !?!'
        
        if (debug) print *, 'DHB, DLOG(AKS), DELE=', &
                             DHB, DLOG(AKS), DELE

      P1=P
      P2=P1-BET*DELE
      E=E-DELE
        if (debug) print *                 
        if (debug) print *,'P1, P2, E=', P1, P2, E

      IF(P2.LT.0.1.OR.E.LT.0.05) THEN
         P=0.          
! Proton is absorbed in the target
         E=0.
	   if (debug) print *, 'IF(P2.LT.0.1.OR.E.LT.0.05)=>', &
        'E=0, P=0, i.e. proton stoped RETURN' 
         RETURN        
! Something to stop the history for the lost proton
      END IF

      P=P2
	if (debug) print *, 'New momentum P=P2=', P

      if (debug) print *, 'Subr. TARGB: CALL SPANB ------'
      CALL SPANB
      TET=SQRT(TET2)
      SS=sin(TET)  !VVK&SIS SS=TET*(1.-TET2/8.)
      CS=cos(TET)  !VVK&SIS CS=1.-0.5*TET2
      if (debug) then
	  print *, 'Subr. TARGB: after CALL SPANB ------'
        Print *, 'TET2,TET,SS,CS=',TET2,TET,SS,CS 
        Print *
        print *, 'check var. MINT=', MINT
        print *, 'Subr. TARGB: before CALL TURNB ------'
      endif

      CALL TURNB 

      if (debug) then
        print *, 'Subr. TARGB: after CALL TURNB ------'
        print *, 'check var. MINT=', MINT
	  print *, 'IF(MINT.EQ.0)RETURN'
      endif

      IF(MINT.EQ.0)RETURN
      RTYP=RNDM5(-1.)
      IF(RTYP-SPAIR/STOT)7,7,9
    7 RR=RNDM5(-1.)
      U=(0.001022/E)**RR
      IF(RNDM5(-1.)-(7.5E-4/(U+7.5E-4))**2)8,8,9
    8 P=P*(1.-U)
      GO TO 1
    9 IF(RTYP-(SPAIR+SEL)/STOT)10,10,14
   10 IF(RNDM5(-1.)-SELI/SEL)11,11,12
   11 BEL=BINE*P*P
      GO TO 13
   12 BEL=BCOH*P*P
   13 RNN11=RNDM5(-1.) 
      if(RNN11)801,801,802
 801  return
 802  TEL2=-DLOG(RNN11)/BEL
      TEL=SQRT(TEL2)
      SS=TEL*(1.-TEL2/8.)
      CS=1.-0.5*TEL2
      CALL TURNB
      GO TO 1
   14 SQS=SQRT(1.87656*(E+0.93828))
      IF(SQS.LT.5.5) THEN
         P=0.
         RETURN
      END IF

      CALL PLEADB
      CALL TURNB
      W=W*V
      E=EN+0.93828
      BET=SQRT(E**2-0.88)/E
      P=BET*E
      GO TO 1
END SUBROUTINE TARGB
!===============================================================

!============================================================
!     ***************************************************************
      SUBROUTINE TURNB
      IMPLICIT DOUBLE PRECISION (A-H,O-U,W-Z)
      IMPLICIT DOUBLE PRECISION(V)
      double precision :: VX,RX,VY,RY,VZ,RZ,G1,G2,GG,G,CH,SH,V22,SS,VCH,VSH, &
                 VCS,CS,VSS,VX1,VDIN,PBDMM,DUMM 
!      REAL RNDM
!- - - - - - - - - - - - - - - - - - - - -
!VK   COMMON/NOBINIT/NOBINI    !  initial turn number for continue run
      COMMON                  &
      /PART/PBDMM(5),RX,RY,RZ &
      /BSELP/DUMM(4),CS,SS
!- - - - - - - - - - - - - - - - - - - - -
      VX=RX
      VY=RY
      VZ=RZ
    1 G1=RNDM5(-1.)
      G2=RNDM5(-1.)
      GG=2.*G1-1.
      G=GG*GG+G2*G2
      IF(G-1.)2,2,1
    2 CH=(GG*GG-G2*G2)/G
      SH=2.*GG*G2/G
      V22=SQRT(VX*VX+VY*VY)
      IF(V22-1.D-3)3,3,4
    3 RX=RX+SS*CH
      RY=RY+SS*SH
      RZ=1.-0.5*(RX**2+RY**2)
      RETURN
    4 VCH=CH
      VSH=SH
      VCS=CS
      VSS=SS
      VX1=VX
      VX=VX*VCS+VSS/V22*(VCH*VX1*VZ-VSH*VY)
      VY=VY*VCS+VSS/V22*(VCH*VY*VZ+VSH*VX1)
      VZ=VZ*VCS-VSS*VCH*V22
    5 VDIN=SQRT(VX*VX+VY*VY+VZ*VZ)
      RX=VX/VDIN
      RY=VY/VDIN
      RZ=VZ/VDIN
      RETURN
END SUBROUTINE TURNB
!     ***************************************************************

!     ***************************************************************
      DOUBLE PRECISION FUNCTION GL(X)
!- - - - - - - - - - - - - - - - - - - - - - - -
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      double precision :: G,X,Y
      integer :: L,IY
      DIMENSION    G(200)
       DATA (G(L),L=1,60)/                      &
     -4.000d0,-2.291d0,-2.105d0,-1.980d0,-1.882d0,-1.800d0, &
     -1.728d0,-1.663d0,-1.604d0,-1.550d0,-1.499d0,-1.450d0, &
     -1.404d0,-1.361d0,-1.319d0,-1.278d0,-1.239d0,-1.201d0, & 
     -1.164d0,-1.128d0,-1.093d0,-1.058d0,-1.024d0,-0.991d0, &
     -0.959d0,-0.927d0,-0.895d0,-0.864d0,-0.833d0,-0.803d0, &
     -0.772d0,-0.743d0,-0.713d0,-0.684d0,-0.655d0,-0.625d0, &
     -0.597d0,-0.568d0,-0.540d0,-0.511d0,-0.483d0,-0.455d0, &
     -0.427d0,-0.399d0,-0.371d0,-0.344d0,-0.316d0,-0.288d0, &
     -0.260d0,-0.233d0,-0.205d0,-0.177d0,-0.150d0,-0.122d0, &
     -0.094d0,-0.067d0,-0.039d0,-0.011d0, 0.017d0, 0.045d0/
       DATA (G(L),L=61,132)/                    &
      0.073d0, 0.101d0, 0.130d0, 0.158d0, 0.187d0, 0.215d0, &
      0.244d0, 0.273d0, 0.302d0, 0.331d0, 0.360d0, 0.390d0, &
      0.419d0, 0.449d0, 0.479d0, 0.509d0, 0.540d0, 0.571d0, &
      0.601d0, 0.632d0, 0.664d0, 0.695d0, 0.727d0, 0.759d0, &
      0.791d0, 0.824d0, 0.857d0, 0.890d0, 0.924d0, 0.957d0, &
      0.992d0, 1.026d0, 1.061d0, 1.096d0, 1.132d0, 1.168d0, &
      1.204d0, 1.241d0, 1.279d0, 1.316d0, 1.354d0, 1.393d0, &
      1.432d0, 1.472d0, 1.512d0, 1.553d0, 1.593d0, 1.635d0, &
      1.678d0, 1.721d0, 1.764d0, 1.808d0, 1.854d0, 1.899d0, &
      1.946d0, 1.992d0, 2.041d0, 2.089d0, 2.139d0, 2.189d0, &
      2.241d0, 2.293d0, 2.346d0, 2.400d0, 2.455d0, 2.511d0, &
      2.569d0, 2.628d0, 2.687d0, 2.749d0, 2.811d0, 2.875d0/
       DATA (G(L),L=133,200)/                   &
      2.940d0, 3.006d0, 3.075d0, 3.145d0, 3.216d0, 3.289d0, &
      3.365d0, 3.442d0, 3.521d0, 3.602d0, 3.686d0, 3.772d0, &
      3.860d0, 3.951d0, 4.045d0, 4.141d0, 4.241d0, 4.344d0, &
      4.450d0, 4.560d0, 4.674d0, 4.791d0, 4.913d0, 5.040d0, &
      5.172d0, 5.308d0, 5.451d0, 5.599d0, 5.754d0, 5.916d0, &
      6.085d0, 6.262d0, 6.447d0, 6.642d0, 6.847d0, 7.063d0, &
      7.291d0, 7.532d0, 7.788d0, 8.059d0, 8.348d0, 8.657d0, &
      8.986d0, 9.340d0, 9.721d0,10.132d0,10.578d0,11.063d0, &
     11.593d0,12.175d0,12.817d0,13.530d0,14.326d0,15.223d0, &
     16.241d0,17.407d0,18.757d0,20.342d0,22.229d0,24.518d0, &
     27.354d0,30.968d0,35.742d0,42.345d0,52.112d0,68.077d0, &
     99.043d0, 0.000d0/
!- - - - - - - - - - - - - - - - - - - - - - - -
      Y=X*(1.+1./99.5)
      GL=1./(1.-X)
      IF(Y.GE.1.) RETURN
      IY=200*Y+1
      IF(IY.GT.198) IY=198
      GL=G(IY)+(G(IY+1)-G(IY))*(200*Y+1.-IY)
      RETURN
END FUNCTION GL
!     ***************************************************************

!     ***************************************************************
      SUBROUTINE SPANB
!- - - - - - - - - - - - - - - - - - - - - - - -
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
!VK   COMMON/NOBINIT/NOBINI    !  initial turn number for continue run
      COMMON /EMSC/P1,P2,BET,Z,A,RO,T,F,TET2
!- - -[P1,P2]=GEV ,[T]=G/CM**2 - - - - - - - - -

      double precision:: F,TET2,Z75,ALP,XAP,XCPT,A,Z43,XCP,XC,T,Z,  & 
                P1,P2,BET,BT,B,FM,ALOM,B1,UM,RNN11,UG,     &
                A1,A2,A3,SA3,RN,G,RO
      F=0.
      TET2=0.
      IF(T.LT.0.001) RETURN
      Z75=(Z/75.)**2
      ALP=7.788E-2*(Z+1.5)**(-0.67)
      XAP=2.02E-11*Z**0.6667
      XCPT=1.58E-7*Z**2/A
      Z43=1130.*Z**(-4./3.)
    2 XCP=XCPT*T
      XC=XCP/(P1*P2*BET)
      BT=DLOG(XCP/(XAP*(BET+Z75)))-0.1544
      BT=BT+(DLOG(Z43*P1*P2/(0.88+1.02E-3*P1))+4.5)/Z
      IF(BT-10.)3,3,4
    3 B=0.7+1.32*BT-0.01451*BT*BT
      GO TO 5
    4 B=BT*(1.+DLOG(BT)/(BT-1.))
    5 FM=ALP/(XCP*B)
      ALOM=DLOG(FM)
      B1=1./B
      IF(FM-4.)6,7,7
    6 UM=1.-(1.4228-ALOM)*B1
      GO TO 8
    7 UM=1.
    8 IF(FM-0.3)9,10,10
    9 RNN11=RNDM5(-1.) 
      F=-UM*DLOG(RNN11)
      TET2=XC*B*F
      RETURN
   10 UG=FM+0.2*ALOM+1.53
      A1=1.-1.25*B1
      A2=1.6728*B1/UM
      A3=2.169169*B1/UM
      SA3=A1+A2+A3
   11 RN=RNDM5(-1.)
      IF(RN-A1/SA3)9,9,12
   12 IF(RN-(A1+A2)/SA3)13,13,14
   13 F=RNDM5(-1.)
      G=0.74725*DEXP(-F/UM)+0.2989*FMOL(F)*DEXP(-F/UG)
      GO TO 15
   14 F=1./(RNDM5(-1.)+1.E-8)
      G=F*F*(0.5763*DEXP(-F/UM)+0.2305*FMOL(F)*DEXP(-F/UG))
   15 IF(RNDM5(-1.)-G)16,16,11
   16 TET2=XC*B*F
      RETURN
END SUBROUTINE SPANB
!     ***************************************************************

      SUBROUTINE PLEADB
! - - - - - - - - - - - - - - - - - - - - - - - - -
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

!VK   COMMON/NOBINIT/NOBINI    !  initial turn number for continue run
      COMMON /BSELP/SQS,EO,EN,V,CS,SS           
      COMMON /MATI/ZMT,AMT,DENS                 
      COMMON /OUTPL/X,Q,PT2,PT,PL,P,E,EC,PCL,W0

      double precision :: PM2,PM,ALA,AMT,VP1,A67,SD1,SQS,S,ALS,V,SPIN,SPIP,  &
                 SRRP,SPPP,SPPR,SPROD,SP1,SQP,SDP,W0,AIN,ADI,AINDI, &                  
                 XI,X,X1,ALX,QMI,QMA,QP,WX,PX,Q,FIN,PX1,C1,C2,ALS3, &
                 EX1,EX2,AN1,AN2,AN,PT2,PCL,EC,PT,G,B,PL,E,EN,P,CS, &
                 SS,DENS,EO,ZMT

      DATA PM2/0.8803693d0/,PM/0.93828d0/
! - - - - - - - - - - - - - - - - - - - - - - - - - 

      ALA=DLOG(AMT)
      VP1=1.073-0.1445*ALA
      A67=(1.-1.25/AMT)*AMT**0.67
      SD1=(1.-DEXP(-0.13*AMT))*AMT**0.315*6.58

      S=SQS*SQS
      ALS=DLOG(S)
      V=0.
      IF(S.LT.30.)RETURN
      SPIN=30.+0.1*SQS
      SPIP=2.-0.75/SQS
      SRRP=1.18-5.7/S
      SPPP=1.288*ALS-0.086*ALS*ALS-1.263
      SPPR=1.433-0.134*ALS
      SPROD=(46.79+0.03*SQS)*A67
      SP1=VP1*SPROD
      SQP=SRRP+SPIP
      SDP=SPPR+SPPP
      W0=(SP1*SQP/SPIN+SD1)/SPROD
      AIN=SP1*SQP
      ADI=SD1*SPIN
      AINDI=AIN+ADI
      AIN=AIN/AINDI
      ADI=ADI/AINDI
      IF(RNDM5(-1.)-AIN)2,2,3
    2 X=0.7+RNDM5(-1.)*(0.3-2./S)
      X1=1.-X
      ALX=DLOG(X1)
      QMI=PM2*X1*X1/X
      QMA=0.5*S*X1-PM2*(2.-X)
      QP=-0.38-1.5*ALX
      WX=(0.3-2./S)*10.367/SRRP/QP*(DEXP(-QP*QMI)-DEXP(-QP*QMA))
      PX=WX/(0.3-2./S)
      Q=-DLOG(DEXP(-QP*QMI)-RNDM5(-1.)*SRRP*PX*QP/10.367)/QP
      FIN=(59.69*Q*X1**(1.+.6*Q)*DEXP(-4.3*Q)/(0.0195+Q)**2+10.367* &
      DEXP(-QP*Q))/SQP
      PX1=10.367*DEXP(-QP*Q)/SRRP
      V=W0*WX*FIN/PX1
      GO TO 10
    3 IF(RNDM5(-1.)-SPPP/SDP)4,4,7
    4 XI=RNDM5(-1.)
      X1=0.3/(0.15*S)**XI
      X=1.-X1
      ALX=DLOG(X1)
      QMI=PM2*X1*X1/X
      QMA=0.5*S*X1-PM2*(2.-X)
      C1=3.94-0.72*ALX
      C2=1.12-0.72*ALX
      ALS3=DLOG(0.15*S)
      EX1=DEXP(-C1*QMI)-DEXP(-C1*QMA)
      EX2=DEXP(-C2*QMI)-DEXP(-C2*QMA)
      AN1=2.32*EX1/C1
      AN2=0.33*EX2/C2
      AN=AN1+AN2
      WX=ALS3/SPPP*(AN1+AN2)
      V=W0*WX
      IF(RNDM5(-1.)-AN1/AN)5,5,6
    5 Q=-DLOG(DEXP(-C1*QMI)-RNDM5(-1.)*AN1*C1/2.32)/C1
      GO TO 10
    6 Q=-DLOG(DEXP(-C2*QMI)-RNDM5(-1.)*AN2*C2/0.33)/C2
      GO TO 10
    7 X=1.-1./(1.82574+RNDM5(-1.)*(SQS/1.4142-1.82574))**2
      X1=1.-X
      ALX=DLOG(X1)
      QMI=PM2*X1*X1/X
      QMA=0.5*S*X1-PM2*(2.-X)
      C1=-0.01-0.72*ALX
      C2=4.41-0.72*ALX
      EX1=DEXP(-C1*QMI)-DEXP(-C1*QMA)
      EX2=DEXP(-C2*QMI)-DEXP(-C2*QMA)
      AN1=0.95*EX1/C1
      AN2=3.47*EX2/C2
      AN=AN1+AN2
      WX=2.*(SQS/1.4142-1.82574)/SPPR/SQS*AN
      V=W0*WX
      IF(RNDM5(-1.)-AN1/AN)8,8,9
    8 Q=-DLOG(DEXP(-C1*QMI)-RNDM5(-1.)*AN1*C1/0.95)/C1
      GO TO 10
    9 Q=-DLOG(DEXP(-C2*QMI)-RNDM5(-1.)*AN2*C2/3.47)/C2
   10 PT2=Q*X-X1*X1*PM2
      PCL=0.5*X*SQS
      EC=SQRT(PT2+PM2+PCL*PCL)
      PT=SQRT(PT2)
      G=SQS*0.5/PM
      B=SQRT(G*G-1.)/G
      PL=G*(PCL+B*EC)
      E=G*(EC+B*PCL)
      EN=E-PM
      P=SQRT(PL*PL+PT2)
      CS=PL/P
      SS=PT/P
      RETURN
END SUBROUTINE PLEADB
!     ***************************************************************
      DOUBLE PRECISION FUNCTION FMOL(X)
!- - - FMOL(X)=2DEXP(-X){(1-X)[LN(X)-EI(X)]+2} - 2 - - - - -
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION XX(19),YY(19)
      double precision :: X,XX,YY,X1,X2,X3,X4,FX,Y,Y1,Y2,Y3
      integer :: I
      DATA XX/1.d0,1.1d0,1.2d0,1.4d0,1.6d0,1.8d0,2.d0,2.2d0, &
      2.4d0,2.6d0,2.8d0,3.d0,4.d0,5.d0,6.d0,7.d0,8.d0,9.d0,10.d0/, &
      YY/0.d0,0.06897d0,.1361d0,.2634d0,.3798d0,.4843d0, &
      0.5767d0,.6574d0,.7271d0,.7868d0,.8373d0,.8798d0,  &
      1.002484d0,1.0397d0,1.0435d0,1.0371d0,1.0292d0,1.0225d0,1.01747d0/
!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
      IF(X-10.)2,2,1
    1 X1=1./X
      X2=X1*X1
      X3=X1*X2
      FMOL=2.*X2*(1.+4.*X1+18.*X2+96.*X3)
      RETURN
    2 IF(X-1.1)3,4,4
    3 X2=X*X
      X3=X2*X
      X4=X3*X
      FX=(X-1.)*(0.5772+X+0.25*X2+5.556E-2*X3+1.04E-2*X4)
      FMOL=2.*DEXP(-X)*(FX+2.)-2.
      RETURN
    4 DO 5 I=2,18
      IF(X-XX(I))6,6,5
    5 CONTINUE
      I=18
    6 X1=XX(I-1)
      X2=XX(I)
      X3=XX(I+1)
      Y1=YY(I-1)
      Y2=YY(I)
      Y3=YY(I+1)
      Y=Y1*(((X-X2)*(X-X3))/((X1-X2)*(X1-X3)))+  &
        Y2*(((X-X1)*(X-X3))/((X2-X1)*(X2-X3)))+  &
        Y3*(((X-X1)*(X-X2))/((X3-X1)*(X3-X2)))
      FMOL=2.*(Y+2.*DEXP(-X)-1.)
      RETURN
END FUNCTION FMOL
!     ***************************************************************

! from rnd.f ====================================================================
!  do it once once at the beginning:
!         IJKLIN = 54217137
!         NTOTIN =        0
!         NTOT2N =        0
!     CALL RM48IN(IJKLIN,NTOTIN,NTOT2N)
!----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION RNDM5 (RDUMMY)                                   
!                                                                               
!----------------------------------------------------------------------*        
!                                                                      *        
!     This routine merely acts as an interface to F.James RM48 gen.    *        
!                                                                      *        
!     Created on 03  april  1992   by    Alfredo Ferrari & Paola Sala  *        
!                                                   Infn - Milan       *        
!                                                                      *        
!     MODIFIED    on 16-sep-93     by    Alfredo Ferrari               *        
!                                                                      *
!     REVISION: 13-FEB-1997    BY NVM                                  *
!                                                                      *
!----------------------------------------------------------------------*        
!                                                                               
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      double precision :: PIPIPI,RNDNUM
      double precision ::  RD2IN, RD2OUT
      integer :: ISEED1,ISEED2,IDUMMY
      REAL RDUMMY
      DIMENSION RNDNUM (2)                                                      
      PARAMETER ( PIPIPI = 3.1415926535897932270D+00 )                         
      CALL RM48 ( RNDNUM, 1 )                                                   
      RNDM5 = RNDNUM (1)                                                         
      RETURN                                                                    
      ENTRY RD2IN (ISEED1,ISEED2)                                               
!  The following card just to avoid warning messages on the HP compiler         
      RD2IN  = PIPIPI                                                           
      CALL RM48IN(54217137,ISEED1,ISEED2)                                      
      RETURN                                                                    
      ENTRY RD2OUT(ISEED1,ISEED2)                                               
!  The following card just to avoid warning messages on the HP compiler         
      RD2OUT = PIPIPI                                                           
      CALL RM48UT (IDUMMY,ISEED1,ISEED2)                                        
      RETURN                                                                    
      END FUNCTION RNDM5                                                        
!-------------------------------------------------------------------
      SUBROUTINE RM48(RVEC,LENV)                                                
!     Double-precision version of                                               
! Universal random number generator proposed by Marsaglia and Zaman             
! in report FSU-SCRI-87-50                                                      
!        based on RANMAR, modified by F. James, to generate vectors             
!        of pseudorandom numbers RVEC of length LENV, where the numbers         
!        in RVEC are numbers with at least 48-bit mantissas.                    
!                                                                      *
!     REVISION: 21-JAN-1997    BY NVM                                  *
!                                                                      *
!   Input and output entry points: RM48IN, RM48UT.                              
!!!! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++         
!!!!  Calling sequences for RM48:                                    ++         
!!!!      CALL RM48 (RVEC, LEN)     returns a vector RVEC of LEN     ++         
!!!!                   64-bit random floating point numbers between  ++         
!!!!                   zero and one.                                 ++         
!!!!      CALL RM48IN(I1,N1,N2)   initializes the generator from one ++         
!!!!                   64-bit integer I1, and number counts N1,N2    ++         
!!!!                  (for initializing, set N1=N2=0, but to restart ++         
!!!!                    a previously generated sequence, use values  ++         
!!!!                    output by RM48UT)                            ++         
!!!!      CALL RM48UT(I1,N1,N2)   outputs the value of the original  ++         
!!!!                  seed and the two number counts, to be used     ++         
!!!!                  for restarting by initializing to I1 and       ++         
!!!!                  skipping N2*100000000+N1 numbers.              ++         
!!!! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      
! for 32-bit machines, use IMPLICIT DOUBLE PRECISION                            

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      integer :: LENV, MODCNS,NTOT,NTOT2,IJKL,LUNOUT,KALLED, &
                 IJKLIN,NTOTIN,NTOT2N,IOSEED,IJKLUT,NTOTUT,  &
                 NTOT2T,IJ,KL,I,J,K,L,II,JJ,M,I24,I97,J97,   &
                 LOOP2,NOW,IDUM,IVEC      
      double precision :: RVEC 
      double precision :: ONE,HALF,ZERO,S,T,U,TWOM24,C,CD,CM,UNI
      DIMENSION RVEC(*)                                                         
      COMMON/R48ST1/U(97),C,I97,J97                                             
      PARAMETER (MODCNS=1000000000)                                             
      SAVE CD, CM, TWOM24,  ZERO, ONE, NTOT, NTOT2, IJKL                        
      DATA NTOT,NTOT2,IJKL/-1,0,0/                                              
      
      DATA LUNOUT/6/ !vk printing unit: DATA LUNOUT/16/
!                                                                               
      IF (NTOT .GE. 0)  GO TO 50                                                
!                                                                               
!        Default initialization. User has called RM48 without RM48IN.           
      IJKL = 54217137                                                           
      NTOT = 0                                                                  
      NTOT2 = 0                                                                 
      KALLED = 0                                                                
      GO TO 1                                                                   
!                                                                               
      ENTRY      RM48IN(IJKLIN, NTOTIN,NTOT2N)                                  
!         Initializing routine for RM48, may be called before                   
!         generating pseudorandom numbers with RM48.   The input                
!         values should be in the ranges:  0<=IJKLIN<=900 OOO OOO               
!                                          0<=NTOTIN<=999 999 999               
!                                          0<=NTOT2N<<999 999 999!              
! To get the standard values in Marsaglia's paper, IJKLIN=54217137              
!                                            NTOTIN,NTOT2N=0                    
      IJKL = IJKLIN                                                             
      NTOT = MAX(NTOTIN,0)                                                      
      NTOT2= MAX(NTOT2N,0)                                                      
      KALLED = 1                                                                
!          always come here to initialize                                       
    1 CONTINUE                                                                  
      IJ = IJKL/30082                                                           
      KL = IJKL - 30082*IJ                                                      
      I = MOD(IJ/177, 177) + 2                                                  
      J = MOD(IJ, 177)     + 2                                                  
      K = MOD(KL/169, 178) + 1                                                  
      L = MOD(KL, 169)                                                          
      WRITE(LUNOUT,'(A,I10,2X,2I10)')         &
       ' RM48 INITIALIZED:',IJKL,NTOT,NTOT2                                     
!        PRINT '(A,4I10)', '   I,J,K,L= ',I,J,K,L                               
      ONE = 1.D+00                                                              
      HALF = 0.5D+00                                                            
      ZERO = 0.D+00                                                             
      DO 2 II= 1, 97                                                            
      S = 0.D+00                                                                
      T = HALF                                                                  
      DO 3 JJ= 1, 48                                                            
         M = MOD(MOD(I*J,179)*K, 179)                                           
         I = J                                                                  
         J = K                                                                  
         K = M                                                                  
         L = MOD(53*L+1, 169)                                                   
         IF (MOD(L*M,64) .GE. 32)  S = S+T                                      
    3    T = HALF*T                                                             
    2 U(II) = S                                                                 
      TWOM24 = ONE                                                              
      DO 4 I24= 1, 24                                                           
    4 TWOM24 = HALF*TWOM24                                                      
      C  =   362436.D+00*TWOM24                                                 
      CD =  7654321.D+00*TWOM24                                                 
      CM = 16777213.D+00*TWOM24                                                 
      I97 = 97                                                                  
      J97 = 33                                                                  
!       Complete initialization by skipping                                     
!            (NTOT2*MODCNS + NTOT) random numbers                               
      DO 45 LOOP2= 1, NTOT2+1                                                   
      NOW = MODCNS                                                              
      IF (LOOP2 .EQ. NTOT2+1)  NOW=NTOT                                         
      IF (NOW .GT. 0)  THEN                                                     
      WRITE(LUNOUT,'(A,I15)') ' RM48IN SKIPPING OVER ',NOW                      
          DO 40 IDUM = 1, NTOT                                                  
          UNI = U(I97)-U(J97)                                                   
          IF (UNI .LT. ZERO)  UNI=UNI+ONE                                       
          U(I97) = UNI                                                          
          I97 = I97-1                                                           
          IF (I97 .EQ. 0)  I97=97                                               
          J97 = J97-1                                                           
          IF (J97 .EQ. 0)  J97=97                                               
          C = C - CD                                                            
          IF (C .LT. ZERO)  C=C+CM                                              
   40     CONTINUE                                                              
      ENDIF                                                                     
   45 CONTINUE                                                                  
      IF (KALLED .EQ. 1)  RETURN                                                
!                                                                               
!          Normal entry to generate LENV random numbers                         
   50 CONTINUE                                                                  
      DO 100 IVEC= 1, LENV                                                      
      UNI = U(I97)-U(J97)                                                       
      IF (UNI .LT. ZERO)  UNI=UNI+ONE                                           
      U(I97) = UNI                                                              
      I97 = I97-1                                                               
      IF (I97 .EQ. 0)  I97=97                                                   
      J97 = J97-1                                                               
      IF (J97 .EQ. 0)  J97=97                                                   
      C = C - CD                                                                
      IF (C .LT. ZERO)  C=C+CM                                                  
      UNI = UNI-C                                                               
      IF (UNI .LT. ZERO) UNI=UNI+ONE                                            
      RVEC(IVEC) = UNI                                                          
!             Replace exact zeros by uniform distr. *2**-24                    
!         IF (UNI .EQ. 0.)  THEN                                                
!         ZUNI = TWOM24*U(2)                                                    
!             An exact zero here is very unlikely, but let's be safe.          
!         IF (ZUNI .EQ. 0.) ZUNI= TWOM24*TWOM24                                 
!         RVEC(IVEC) = ZUNI                                                     
!         ENDIF                                                                 
  100 CONTINUE                                                                  
      NTOT = NTOT + LENV                                                        
         IF (NTOT .GE. MODCNS)  THEN                                            
         NTOT2 = NTOT2 + 1                                                      
         NTOT = NTOT - MODCNS                                                   
         ENDIF                                                                  
      RETURN                                                                    
!           Entry to output current status                                      
      ENTRY RM48UT(IJKLUT,NTOTUT,NTOT2T)                                        
      IJKLUT = IJKL                                                             
      NTOTUT = NTOT                                                             
      NTOT2T = NTOT2                                                            
!NVM
      WRITE(LUNOUT,'(//A,I10,2X,2I10)')   &
       ' RM48UT OUTPUT:',IJKL,NTOT,NTOT2                                     
!NVM
      RETURN                                                                    
!                                                                               
      ENTRY      RM48WR(IOSEED)                                                 
!         Output routine for RM48, without skipping numbers                     
      WRITE (IOSEED,'(2Z8)') NTOT,NTOT2                                         
      WRITE (IOSEED,'(2Z8,Z16)') I97,J97,C                                      
      WRITE (IOSEED,'(24(4Z16,/),Z16)') U                                       
      RETURN                                                                    
!                                                                               
      ENTRY      RM48RD(IOSEED)                                                 
!         Initializing routine for RM48, without skipping numbers               
      READ (IOSEED,'(2Z8)') NTOT,NTOT2                                          
      READ (IOSEED,'(2Z8,Z16)') I97,J97,C                                       
      READ (IOSEED,'(24(4Z16,/),Z16)') U                                        
      CLOSE (UNIT=IOSEED)                                                       
      IJKL = 54217137                                                           
      IJ = IJKL/30082                                                           
      KL = IJKL - 30082*IJ                                                      
      I = MOD(IJ/177, 177) + 2                                                  
      J = MOD(IJ, 177)     + 2                                                  
      K = MOD(KL/169, 178) + 1                                                  
      L = MOD(KL, 169)                                                          
      WRITE (LUNOUT,'(A,I10,2X,2I10)')         &
        ' RM48 INITIALIZED:',IJKL,NTOT,NTOT2                                    
!         PRINT '(A,4I10)', '   I,J,K,L= ',I,J,K,L                               
      ONE  = 1.D+00                                                             
      HALF = 0.5D+00                                                            
      ZERO = 0.D+00                                                             
      TWOM24 = ONE                                                              
      DO 400 I24= 1, 24                                                         
  400 TWOM24 = HALF*TWOM24                                                      
      CD =  7654321.D+00*TWOM24                                                 
      CM = 16777213.D+00*TWOM24                                                 
      RETURN                                                                    
  END SUBROUTINE RM48
!---------------------------------------------------

END MODULE TARGB_STRUCT
