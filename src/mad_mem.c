#include <stdio.h>
#include <stdlib.h>
//hrr
#include <unistd.h>
#include <execinfo.h>
#include <signal.h>

#include "mad_err.h"
#include "mad_mem.h"

// public interface

void
mad_mem_handler(int sig)
{
  (void)sig;
  void *array[20];
  size_t size;
  size = backtrace(array, 20);
  
  puts("+++ memory access outside program range, fatal +++");

//hrr
//backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(EXIT_FAILURE);
}

void*
myptrchk(const char *caller, void *ptr)
{
  if (!ptr)
    fatal_error("memory overflow, called from routine:", caller);

  return ptr;
}
